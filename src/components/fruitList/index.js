const FruitList = ({ fruits }) => {
    return (
        <div>
            <ul>
                {fruits.map((fruit) => {
                    return (
                    <li>
                        {fruit.name}
                    </li>
                    )
                })}
            </ul>
        </div>
    );
}

export default FruitList;