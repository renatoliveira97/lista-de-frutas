import './App.css';

import { useState } from 'react';

import FruitList from './components/fruitList';

const App = () => {

  const [fruits, setFruits] = useState([
    { name: "banana", color: "yellow", price: 2 },
    { name: "cherry", color: "red", price: 3 },
    { name: "strawberry", color: "red", price: 4 },
  ]);

  const filterRedFruits = () => {
    const arr = fruits.filter(f => f.color === "red");
    setFruits(arr);
  }

  return (
    <div className="App">
      <header className="App-header">
        <p>Preço total = {fruits.reduce((acc, fruit) => acc + fruit.price, 0)}</p>
        <FruitList fruits={fruits}/>
        <button className="btnRedFruits" onClick={filterRedFruits}>Mostrar frutas vermelhas</button>
      </header>
    </div>
  );
}

export default App;
